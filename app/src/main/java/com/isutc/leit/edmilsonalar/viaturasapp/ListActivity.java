package com.isutc.leit.edmilsonalar.viaturasapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ListView listView = (ListView) findViewById(R.id.list);

        Intent intent  = getIntent();
        Bundle bundle = intent.getBundleExtra("BUNDLE");
        ArrayList<Vehicle> vehicles = (ArrayList<Vehicle>) bundle.getSerializable("LIST");
        VehicleAdapter adapter = new VehicleAdapter(this,vehicles);
        listView.setAdapter(adapter);
    }
}