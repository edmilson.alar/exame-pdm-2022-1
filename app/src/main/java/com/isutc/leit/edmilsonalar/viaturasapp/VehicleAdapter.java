package com.isutc.leit.edmilsonalar.viaturasapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class VehicleAdapter extends BaseAdapter {

    Context context;
    ArrayList<Vehicle> vehicles;
    public VehicleAdapter(Context context, ArrayList<Vehicle> vehicles) {
        this.context = context;
        this.vehicles = vehicles;
    }

    @Override
    public int getCount() {
        return vehicles.size();
    }

    @Override
    public Object getItem(int i) {
        return vehicles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return vehicles.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_list, viewGroup, false);
        }
        TextView tvMarca = view.findViewById(R.id.tv_marca);
        TextView tvModelo = view.findViewById(R.id.tv_modelo);
        TextView tvVelocidadeMaxima = view.findViewById(R.id.tv_velocidade_maxima);
        TextView tvAnoFabrico = view.findViewById(R.id.tv_ano_fabrico);
        Vehicle viatura = vehicles.get(i);
        tvMarca.setText(viatura.getBrand());
        tvModelo.setText(viatura.getModel());
        tvAnoFabrico.setText(String.valueOf(viatura.getYear()));
        tvVelocidadeMaxima.setText(viatura.getTopSpeed()+" Km/h");
        return view;
    }


}
