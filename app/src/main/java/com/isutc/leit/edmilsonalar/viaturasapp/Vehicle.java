package com.isutc.leit.edmilsonalar.viaturasapp;

import java.io.Serializable;

public class Vehicle implements Serializable {
    private long id;
    private String brand;
    private  String model;
    private  String year;
    private  String topSpeed;

    public Vehicle(long id, String brand, String model, String year, String topSpeed) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.topSpeed = topSpeed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(String topSpeed) {
        this.topSpeed = topSpeed;
    }
}
