package com.isutc.leit.edmilsonalar.viaturasapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class FormActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner spBrand;
    EditText edtModel,edtYear,edtTopSpeed;
    Button btSave,btBack;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        spBrand = findViewById(R.id.sp_brand);
        String[] marcas = {"TOYOTA", "NISSAN", "MAZDA"};
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, marcas);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBrand.setAdapter(spinnerAdapter);

        edtModel = (EditText) findViewById(R.id.edt_model);
        edtYear = (EditText) findViewById(R.id.edt_year);
        edtTopSpeed = (EditText) findViewById(R.id.edt_topSpeed);

        btBack = findViewById(R.id.bt_back);
        btSave = findViewById(R.id.btSave);

        btSave.setOnClickListener(this);
        btBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if ( view.equals(btSave) ){
            DatabaseHelper dbHelper = new DatabaseHelper(this);
            String brand = spBrand.getSelectedItem().toString();
            String model = edtModel.getText().toString();
            String year = edtYear.getText().toString();
            String topSpeed = edtTopSpeed.getText().toString();
            if(
                   brand.isEmpty() || model.isEmpty() || year.isEmpty() || topSpeed.isEmpty()
            ){
                Toast.makeText(this, "Por favor preencha todos campos", Toast.LENGTH_SHORT).show();

            }else{
                dbHelper.addVehicle(
                        brand,
                        model,
                        year,
                        topSpeed);
            }

        }
        if ( view.equals(btBack) ){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}