package com.isutc.leit.edmilsonalar.viaturasapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "viatura.db";
    private static final int DATABASE_VERSION = 3;

    private static final String TABLE_NAME = "vehicle";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_BRAND = "brand";
    private static final String COLUMN_MODEL = "model";
    private static final String COLUNN_YEAR = "year";
    private static final String COLUMN_TOP_SPEED = "top_speed";

    private Context context;



    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + " ("+
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "+
                COLUMN_BRAND + " TEXT , "+
                COLUMN_MODEL + " TEXT , "+
                COLUNN_YEAR + " INTEGER , "+
                COLUMN_TOP_SPEED + " INTEGER " +
                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        this.onCreate(db);
    }

    void addVehicle( String brand, String model, String year, String topSpeed){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(COLUMN_BRAND, brand);
        cv.put(COLUMN_MODEL, model);
        cv.put(COLUNN_YEAR, year);
        cv.put(COLUMN_TOP_SPEED, topSpeed);

        long resutl = db.insert(TABLE_NAME, null, cv);

        if( resutl == -1 ){
            Toast.makeText(context, "Ocorreu um problema ao adicionar viatura", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "Viatura adicionada", Toast.LENGTH_SHORT).show();
        }
    }

    public Cursor getAllVehicles(){

        String query = "SELECT * FROM "+ TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        return cursor;
    }

    public Cursor getFastestVehicle(){

        String query = "SELECT * FROM "+ TABLE_NAME +" order by "+COLUMN_TOP_SPEED+" Desc Limit 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        return cursor;
    }
    public Cursor getOldestVehicle(){

        String query = "SELECT * FROM "+ TABLE_NAME +" order by "+COLUNN_YEAR+" Asc Limit 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        return cursor;
    }

    public float getToyotaSpeedAverage(){

        String query = "SELECT sum("+COLUMN_TOP_SPEED+")/count(*) as average FROM "+ TABLE_NAME +" Where "+COLUMN_BRAND+"='TOYOTA'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToNext();
        float average = cursor.getFloat(0);

        return average;
    }

    public float speedAverage(){

        String query = "SELECT sum("+COLUMN_TOP_SPEED+")/count(*) as average FROM "+ TABLE_NAME ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToNext();
        float average = cursor.getFloat(0);

        return average;
    }
}
