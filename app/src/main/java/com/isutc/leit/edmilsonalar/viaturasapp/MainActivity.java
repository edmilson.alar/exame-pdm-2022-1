package com.isutc.leit.edmilsonalar.viaturasapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btRegister,btList,btOldest,btFastest,btAverage,btAverageToyota;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(this);

        btRegister = (Button) findViewById(R.id.btForm);
        btList = (Button) findViewById(R.id.btList);
        btFastest = (Button) findViewById(R.id.btFastest);
        btOldest = (Button) findViewById(R.id.btOldest);
        btAverageToyota = (Button) findViewById(R.id.btAverageToyota);
        btAverage = (Button) findViewById(R.id.btAverage);

        btRegister.setOnClickListener(this);
        btList.setOnClickListener(this);
        btFastest.setOnClickListener(this);
        btOldest.setOnClickListener(this);
        btAverageToyota.setOnClickListener(this);
        btAverage.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Cursor cursor;
        Intent intent;
        Bundle bundle = new Bundle();;
        ArrayList<Vehicle> vehicles=new ArrayList<>();
        if(view.equals(btRegister)){
            intent = new Intent(this,FormActivity.class);

            startActivity(intent);
        }
        else if(view.equals(btList)){
            intent = new Intent(this,ListActivity.class);

            vehicles = cursorToList(db.getAllVehicles());
            bundle.putSerializable("LIST",vehicles);
            intent.putExtra("BUNDLE",bundle);

            startActivity(intent);
        }
        else if(view.equals(btFastest)){
            intent = new Intent(this,ListActivity.class);

            vehicles = cursorToList(db.getFastestVehicle());
            bundle.putSerializable("LIST",vehicles);
            intent.putExtra("BUNDLE",bundle);

            startActivity(intent);
        }
        else if(view.equals(btOldest)){
            intent = new Intent(this,ListActivity.class);

            vehicles = cursorToList(db.getOldestVehicle());
            bundle.putSerializable("LIST",vehicles);
            intent.putExtra("BUNDLE",bundle);

            startActivity(intent);
        }
        else if(view.equals(btAverage)){

            Toast.makeText(this, "Velocidade Media: "+db.speedAverage(), Toast.LENGTH_SHORT).show();
        }
        else if(view.equals(btAverageToyota)){
            Toast.makeText(this, "Velocidade Media: "+db.getToyotaSpeedAverage(), Toast.LENGTH_SHORT).show();
        }

    }

    private ArrayList cursorToList(Cursor cursor){
       ArrayList<Vehicle> vehicles = new ArrayList<>();
        while(cursor.moveToNext()){
            Vehicle vehicle = new Vehicle(
                    Long.parseLong(cursor.getString(0)) ,
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)

            );
            vehicles.add(vehicle);
        }
        return  vehicles;
    }


}